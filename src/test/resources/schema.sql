-- hsql

CREATE TABLE Messages
(
    id     INTEGER IDENTITY NOT NULL,
    author VARCHAR(32),
    text   VARCHAR(128),
    date   DATETIME
);

