package be.wardTruyen.spring.restmessages.services;

import be.wardTruyen.spring.restmessages.RestMessagesApplication;
import be.wardTruyen.spring.restmessages.model.Message;
import be.wardTruyen.spring.restmessages.repositories.MessagesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = RestMessagesApplication.class)
@Transactional
class MessagesRepositoryTest {

   @Autowired
   MessagesRepository dao;

   @Test
   void testGetByID() {
      Message m = dao.getById(1);

      assertEquals(1, m.getId());
      assertEquals("Homer", m.getAuthor());
      assertEquals("The Simpsons rule", m.getText());
      assertEquals(LocalDateTime.of(2020, 9, 22, 13, 10, 0), m.getDate());
   }

   @Test
   void testGetAll() {
      List<Message> messageList = dao.findAll();
      assertEquals(3, messageList.size());
   }
}
