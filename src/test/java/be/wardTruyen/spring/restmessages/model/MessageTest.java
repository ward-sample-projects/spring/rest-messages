package be.wardTruyen.spring.restmessages.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class MessageTest {
   @Test
   void createMessageTest(){
      final int TEST_ID = 100;
      final String TEST_AUTHOR = "Test";
      final String TEST_TEXT = "Hello there";
      final LocalDateTime TEST_DATE = LocalDateTime.now();

      Message m = new Message(TEST_ID, TEST_AUTHOR, TEST_TEXT, TEST_DATE);

      assertEquals(TEST_ID, m.getId());
      assertEquals(TEST_AUTHOR, m.getAuthor());
      assertEquals(TEST_TEXT, m.getText());
      assertEquals(TEST_DATE, m.getDate());
   }
}
