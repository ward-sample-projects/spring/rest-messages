function getAllMessages() {
   const xhr = new XMLHttpRequest();
   xhr.onload = loadAllMessages;
   xhr.open("GET", "messages", true);
   xhr.setRequestHeader("Accept", "application/json");
   xhr.send(null);
}

function loadAllMessages() {
   const messagesDiv = document.getElementById("messages")

   switch (this.status) {
      case 200:
         const messageList = JSON.parse(this.responseText);
         // console.log(messageList);
         messagesDiv.innerText = ""
         messageList.reverse();
         for (let message of messageList) {
            // console.log("author: " + message.author);
            let cnt = document.createElement("div");
            cnt.setAttribute("class", "messageDiv")
            let aut = document.createElement("div");
            let date = document.createElement("div");
            date.setAttribute("class", "float-right")
            message.date = message.date.replace("T", " ");
            message.date = message.date.replace(/[.]\d+\b/, "");
            date.innerHTML = message.date;
            aut.appendChild(date);
            aut.innerHTML += "author: " + message.author;// + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Date: " + message.date;
            let msg = document.createElement("div");
            msg.innerText = "message: " + message.text;

            cnt.appendChild(aut);
            cnt.appendChild(msg);

            messagesDiv.appendChild(cnt);
         }
         break;
      case 404:
         messagesDiv.innerText = "Messages not found";
         break;
      default:
         messagesDiv.innerText = "Error: " + this.status;
   }
}

function postForm(e) {
   const xhr = new XMLHttpRequest();
   xhr.onload = postResponse;
   xhr.open("POST", "messages", true);
   xhr.setRequestHeader("content-type", "application/json");
   let authorValue = document.getElementById("author").value;
   let textValue = document.getElementById("text").value;
   let jsonObject = {author: authorValue, text: textValue};
   console.log(jsonObject);
   let jsonString = JSON.stringify(jsonObject);
   console.log(jsonString);
   xhr.send(jsonString);
   e.preventDefault();
}

function postResponse() {
   switch (this.status) {
      case 200:
      case 201:
         // when post was successful, refresh messages
         console.log("POST ok: getting messages again")
         getAllMessages();
         // clear form input fields
         document.getElementById("author").value = '';
         document.getElementById("text").value = '';
         // scroll to the top of the document
         document.body.scrollTop = 0; // For Safari
         document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
         break;
      default:
         console.log("error: " + this.status);
   }
}

function init() {
   getAllMessages();
   // override submit button to our XMLHttpRequest/AJAX-postForm
   document.getElementById("myForm").addEventListener("submit", postForm)
}

window.addEventListener("load", init);
