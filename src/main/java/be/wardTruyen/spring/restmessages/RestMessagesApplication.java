package be.wardTruyen.spring.restmessages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class RestMessagesApplication {
   public static void main(String[] args) {
      SpringApplication.run(RestMessagesApplication.class, args);
   }
}
