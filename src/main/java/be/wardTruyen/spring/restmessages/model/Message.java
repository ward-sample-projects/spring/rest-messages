package be.wardTruyen.spring.restmessages.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "Messages")
@XmlRootElement( name ="Message")
public class Message implements Serializable {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   private String author;
   private String text;
   private LocalDateTime date;

   public Message() {
   }

   public Message(Message copy) {
      this.id = copy.getId();
      this.author = copy.getAuthor();
      this.text = copy.getText();
      this.date = copy.getDate();
   }

   public Message(int id, String author, String text, LocalDateTime date) {
      this.id = id;
      this.author = author;
      this.text = text;
      this.date = date;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getAuthor() {
      return author;
   }

   public void setAuthor(String author) {
      this.author = author;
   }

   public String getText() {
      return text;
   }

   public void setText(String text) {
      this.text = text;
   }

   public LocalDateTime getDate() {
      return date;
   }

   public void setDate(LocalDateTime date) {
      this.date = date;
   }

   @Override
   public String toString() {
      return "Message{" +
              "id=" + id +
              ", author='" + author + '\'' +
              ", text='" + text + '\'' +
              ", date=" + date +
              '}';
   }
}
