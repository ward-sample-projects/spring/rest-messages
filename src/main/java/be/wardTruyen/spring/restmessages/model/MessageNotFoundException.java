package be.wardTruyen.spring.restmessages.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( reason = "Message not found", code= HttpStatus.NOT_FOUND)
public class MessageNotFoundException extends RuntimeException {
   public MessageNotFoundException() {
   }

   public MessageNotFoundException(String message) {
      super(message);
   }

   public MessageNotFoundException(String message, Throwable cause) {
      super(message, cause);
   }

   public MessageNotFoundException(Throwable cause) {
      super(cause);
   }

   public MessageNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
