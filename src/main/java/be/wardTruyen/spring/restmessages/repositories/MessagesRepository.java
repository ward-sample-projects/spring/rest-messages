package be.wardTruyen.spring.restmessages.repositories;

import be.wardTruyen.spring.restmessages.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessagesRepository extends JpaRepository<Message, Integer> {
}