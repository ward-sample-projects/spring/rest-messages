package be.wardTruyen.spring.restmessages.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
@ResponseBody
public class TestRestEndpoint {

   @GetMapping( value="{number:^\\d+$}")
   String getHandlerIntegers(@PathVariable("number") int number){
      return String.valueOf(number * 2);
   }

   @GetMapping( value="{name:^\\p{Alpha}+$}")
   String getHandlerNames(@PathVariable("name") String name){
      return "Hello " + name;
   }
}
