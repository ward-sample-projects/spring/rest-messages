package be.wardTruyen.spring.restmessages.rest;

import be.wardTruyen.spring.restmessages.model.Message;
import be.wardTruyen.spring.restmessages.model.MessageNotFoundException;
import be.wardTruyen.spring.restmessages.repositories.MessagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/messages")
@Transactional
public class MessageRestEndpoint {

   MessagesRepository dao;

   @Autowired
   public void setDao(MessagesRepository dao) {
      this.dao = dao;
   }

   @GetMapping(value = "{id:^\\d+$}",
         produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Message> getMessage(@PathVariable("id") int id) {
      try {
         Message m = dao.getById(id);
         return new ResponseEntity<>(new Message(m), HttpStatus.OK);
      } catch (EntityNotFoundException enfe) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//         throw new MessageNotFoundException(enfe);
      } catch (Exception e) {
         //System.out.println("WTF happened!?!" + e);
         //return new ResponseEntity<>(HttpStatus.CONFLICT);
         throw new MessageNotFoundException(e);
      }
   }

   @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<List<Message>> getMessages() {
      List<Message> messages = dao.findAll();
      return new ResponseEntity<>(messages, HttpStatus.OK);
   }

   @PostMapping(consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
         produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
   public ResponseEntity<Message> postMessage(@RequestBody Message m) {
      System.out.println(m);
      if(m.getAuthor() == null || m.getAuthor().isBlank()) throw new DataIntegrityViolationException("Message.author empty");
      if(m.getText() == null || m.getText().isBlank()) throw new DataIntegrityViolationException("Message.text empty");
      m.setDate(LocalDateTime.now());
      m = dao.save(m);
      return new ResponseEntity<>(new Message(m), HttpStatus.OK);
   }
}
